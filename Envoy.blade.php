@servers(['web' => 'timothy@adsd2019.clow.nl'])

@setup
$gitUrl = 'git@gitlab.com:Timothyvankeulen/scrumapp.git';
$branch = (!empty(@branch)) ? $branch : 'master';
$basePath = '/home/timothy/public_html/scrumapp';
@endsetup

@task('deploy:cold')
cd {{ $basePath }}
git init
git remote add origin {{ $gitUrl }}
git fetch
git checkout {{ $branch }}
git pull origin {{ $branch }}
composer install
npm install
npm run dev
php artisan storage:link
php artisan migrate
chmod -R 777 storage
@endtask

@task('deploy')
cd {{ $basePath }}
git fetch
git checkout {{ $branch }}
git pull origin {{ $branch }}
composer install
npm install
npm run dev
php artisan migrate
chmod -R 777 storage
@endtask