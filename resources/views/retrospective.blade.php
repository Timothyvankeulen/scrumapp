@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="mb-3">
                    <div class="card">
                        <form action="{{ route('retroSave') }}" method="post">
                            @csrf
                            <div class="card-header">
                                Ging Goed: <br><textarea name="went_well" cols="50" required></textarea><br>
                                Ging Slecht: <br><textarea name="went_bad" cols="50" required></textarea>
                            </div>
                            <div class="card-body">
                                Verbeteringen: <br><textarea name="improvement" rows="4" cols="50"
                                                           required></textarea><br>
                                <input type="submit" value="opslaan">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            @foreach($retrospectives as $retrospective )
                <div class="col-sm-4 mb-3">
                    <div class="card">
                        <div class="card-header">
                            {{ $retrospective->id }}
                        </div>
                        <div class="card-body">
                            <p>Went Well: {{ $retrospective->went_well }}</p>
                            <p>Went Bad: {{ $retrospective->went_bad }}</p>
                            <p>Improvement: <br> {{ $retrospective->improvement }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection