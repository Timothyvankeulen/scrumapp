@extends('layouts.app')

@section('content')
    <div class="container text-center"><h4>Mijn projecten</h4></div>
    <div class="container">
        <div class="row justify-content-center">
            @foreach($projects as $project)
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            {{ $project->title }}
                        </div>
                        <div class="card-body">
                            Omschrijving project :{{ $project->description }}
                            <br>
                            Deadline :{{ $project->deadline }}
                        </div>
                    </div>
                    <br>
                </div>
            @endforeach
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div id="paginationEvents" style="text-align: center">
                    <div id="bar">
                        {{ $projects->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--create project--}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5">
                <div class="card">
                    <form action="{{ route('project.store') }}" method="post">
                        @csrf
                        <div class="card-header">
                            <a>Naam project </a><input type="text" name="title" placeholder="Scrum" required><br><br>
                            <a>Beschrijving </a><textarea name="description" placeholder="beschrijving project"></textarea>
                            {{--<input type="text" name="activity" required>--}}
                        </div>
                        <div class="card-body">
                            Deadline: <input type="date" name="deadline" placeholder="2020-01-10" required><br><br>
                            <div class="text-center">
                                <input type="submit" value="Maak nieuw project">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{--end create project--}}
@endsection