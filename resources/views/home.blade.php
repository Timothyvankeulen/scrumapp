@extends('layouts.app')

@section('content'){{--create backlog item --}}
<div class="container text-center"><h3>Voeg een backlog item toe</h3></div>
<div class="row mt-5 justify-content-center">
    <div class="col-6">
        <div class="card">
            <form action="{{ route('backlogItemSave') }}" method="post">
                @csrf
                <div class="card-header">
                    Als <input type="text" name="role" placeholder="admin" required>
                    wil ik  <input type="text" name="activity" placeholder="story" required>
                </div>
                <div class="card-body">
                    Story points: <input type="number" name="story_points" required><br>
                    <div class="text-center"><input type="submit" value="opslaan"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<br>
{{--view backlogitems on board--}}
<div class="container text-center"><h4>Backlogitems</h4></div>
<div class="container">
    <div class="row justify-content-center">
        @foreach($backlogItems as $backlogItem)
            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        Als {{ $backlogItem->role }} wil ik {{ $backlogItem->activity }}
                    </div>
                    <div class="card-body">
                        Story points: {{ $backlogItem->story_points }}
                    </div>
                    <div>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#removeModal{{$backlogItem->id}}">
                            X
                        </button>
                    </div>
                    <div class="modal" id="removeModal{{$backlogItem ->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Weet je zeker dat je <b>{{ $backlogItem ->activity }}</b> wil verwijderen?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                    <form action="{{url('removebli')}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="id" value="{{$backlogItem ->id}}">
                                        <input type="submit" value="Verwijder" class="btn btn-danger">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        @endforeach
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div id="paginationEvents" style="text-align: center">
                <div id="bar">
                {{ $backlogItems->links() }}
                </div>
            </div>
        </div>
    </div>
    {{--end backlogitems--}}
</div>
{{--end create backlogitem--}}
@endsection